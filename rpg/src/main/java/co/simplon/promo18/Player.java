package co.simplon.promo18;

public class Player {

  private String className;
  private String name;
  private int lvl = 1;
  private int currentHP;
  private int maxHP;
  private int atk;
  private int def;
  private int xp;
  private double xpNextLvl = 100;

  // constructeur
  public Player(String className, String name, int maxHP, int atk, int def) {
    this.className = className;
    this.name = name;
    this.maxHP = maxHP;
    this.atk = atk;
    this.def = def;
  }

  // getters et setters
  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getLvl() {
    return lvl;
  }

  public void setLvl(int lvl) {
    this.lvl = lvl;
  }

  public int getCurrentHP() {
    return currentHP;
  }

  public void setCurrentHP(int currentHP) {
    this.currentHP = currentHP;
  }

  public int getMaxHP() {
    return maxHP;
  }

  public void setMaxHP(int maxHP) {
    this.maxHP = maxHP;
  }

  public int getXp() {
    return xp;
  }

  public void setXp(int xp) {
    this.xp = xp;
  }

  public int getAtk() {
    return atk;
  }

  public void setAtk(int atk) {
    this.atk = atk;
  }

  public int getDef() {
    return def;
  }

  public void setDef(int def) {
    this.def = def;
  }

  public double getXpNextLvl() {
    return xpNextLvl;
  }

  public void setXpNextLvl(double xpNextLvl) {
    this.xpNextLvl = xpNextLvl;
  }

}
