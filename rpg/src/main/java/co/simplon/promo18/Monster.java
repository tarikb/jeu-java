package co.simplon.promo18;

public class Monster {
    public String name;
    public int lvl;
    public int currentHP;
    public int maxHP;
    public int xp;
    public int atk;
    public int def;

    public Monster(String name, int lvl, int maxHP, int atk, int def, int xp) {
        this.name = name;
        this.lvl = lvl;
        this.maxHP = maxHP;
        this.atk = atk;
        this.def = def;
        this.xp = xp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        this.currentHP = currentHP;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }
}
