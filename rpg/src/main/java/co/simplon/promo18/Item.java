package co.simplon.promo18;

public class Item {
    private String nameItem;
    private int numberItem;
    private int currentNumberitem;
    private int healthEffect;
    private int atkEffect;
    private int defEffect;
    
    public Item(String nameItem, int numberItem, int currentNumberitem, int healthEffect, int atkEffect, int defEffect) {
        this.nameItem = nameItem;
        this.numberItem = numberItem;
        this.currentNumberitem = currentNumberitem;
        this.healthEffect = healthEffect;
        this.atkEffect = atkEffect;
        this.defEffect = defEffect;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public int getNumberItem() {
        return numberItem;
    }

    public void setNumberItem(int numberItem) {
        this.numberItem = numberItem;
    }

    public int getCurrentNumberitem() {
        return currentNumberitem;
    }

    public void setCurrentNumberitem(int currentNumberitem) {
        this.currentNumberitem = currentNumberitem;
    }

    public int gethealthEffect() {
        return healthEffect;
    }

    public void sethealthEffect(int healthEffect) {
        this.healthEffect = healthEffect;
    }

    public int getatkEffect() {
        return atkEffect;
    }

    public void setatkEffect(int atkEffect) {
        this.atkEffect = atkEffect;
    }

    public int getdefEffect() {
        return defEffect;
    }

    public void setdefEffect(int defEffect) {
        this.defEffect = defEffect;
    }
}