# Projet jeu Java


## Jeu de style RPG

Le jeu que j'ai choisis est un RPG. Le jeu ne comprend pas  d'histoire mais je le classe quand même dans la catégorie RPG par rapport au système de combat utilisé.

## Fonctionnement du jeu

Le jeu se base sur un système de combat style RPG "classique" au tour par tour, avec un système d'objet, de classe et de monté de compétences du personnage.

Lors du lancement de la partie, le joueur est invité à entrer un nom de joueur, la suite des instructions permettant d'interagir avec les différents menu est indiqué.

Le principe du jeu est d’enchaîner les combat dans le but de prendre des niveaux afin d'augmenter ses caractéristiques et d’affronter des monstres toujours plus puissants.

## Explication du code

Dans le but de rendre le code plus lisible, j'ai essayer de séparer au maximum des parties de code afin de les regrouper dans différentes méthodes pouvant êtres utilisées plusieurs fois. 

J'ai créer une classe Personnage et une classe Monstre qui comprendront leurs propres caractéristiques. J'ai aussi créer une classe Game qui elle comprend la logique du jeu et me permet d'instancier mes monstres et mon personnage.

Lorsque l'utilisateur choisit une classe, j'instancie l'objet de ma classe Personnage qui comprend des caractéristiques différentes selon la classe choisie.

Concernant les monstres je créer des instances différentes et je laisse l'utilisateur choisir son adversaire.

Pour l'utilisation des objets j'ai créer une liste contenant différents objets qui se supprime en fonction de l'utilisation de l'objet et du nombre restant de celui-ci

Différentes méthodes sont appelé en fonction du choix de l'utilisateur et de la phase dans laquelle il se trouve.
Par exemple la classe Fight est appelé lorsque celui-ci démarre un combat, et la méthode Items est appelé lorsque l'utilisateur décide d'aller dans es objets.

## Améliorations futures

Au niveau des fonctionnalités plusieurs choses peuvent être améliorées ou ajoutées.

### Exemples d'ameliorations et d'ajouts

- Ajouter un système d'équipement 
- Ajouter un système de skill consommant des points de magies
- Ajouter plus de classes pour le personnage et plus de monstre
- Inclure un shop pour dépenser des Gold contres des objets ou équipements (Gold gagnés en battant les monstres)
- Refactorisation de code en segmentant encore plus le code, ajoutant ainsi de nouvelles méthodes 

