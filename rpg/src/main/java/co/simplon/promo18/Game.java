package co.simplon.promo18;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
  Scanner scanner = new Scanner(System.in);
  Player player = new Player("Joueur", "", 1, 100, 1);
  Monster monster = new Monster("Slime", 1, 50, 3, 4, 50);

  List<Item> itemList = new ArrayList<Item>();
  Item healthPotion = new Item("Potion de vie", 3, 3, 100, 0, 0);
  Item forcePotion = new Item("Potion de force", 3, 3, 0, 20, 0);
  Item defPotion = new Item("Potion de défense", 3, 3, 0, 0, 20);
  Item superPotion = new Item("Super potion", 1, 1, 100, 20, 20);

  public void start() {
    System.out.println("-------------Bonjour!--------------*");
    choiceName();
    choiceClass();
    menu();
  }

  // Menu
  public void menu() {
    System.out.println("--------------------------Menu--------------------------");
    System.out.println("Bonjour " + player.getName());
    System.out.println("1. Démarrer l'aventure");
    System.out.println("2. Statistiques");
    System.out.println("3. Quitter la partie");
    switch (scanner.nextInt()) {
      case 1:
        selectMonster();
        break;
      case 2:
        playerInfo();
        break;
      case 3:
        System.out.println("Merci d'avoir joué !");
        System.exit(0);
        break;
    }
  }

  public void playerInfo() {
    System.out.println("--------------------------Statistiques de " + player.getName() + " --------------------------");
    System.out
        .println("Classe : " + player.getClassName() + " Niveau : " + player.getLvl() + " PV : " + player.getMaxHP()
            + " Attaque : " + player.getAtk() + " Défense : " + player.getDef());
    System.out.println("1---Retour");
    if (scanner.nextInt() == 1) {
      menu();
    }
  }

  public void selectMonster() {
    System.out.println("Qui souhaites-tu affronter ???");
    System.out.println("1. Slime");
    System.out.println("2. Chient errant");
    System.out.println("3. Serpent nocturne");
    System.out.println("4. Dragon de mer");
    System.out.println("5. Retourner au menu");

    switch (scanner.nextInt()) {
      case 1:
        monster = new Monster("Slime", 1, 50, 3, 4, 50);
        break;
      case 2:
        monster = new Monster("Chient errant", 5, 100, 3, 4, 150);
        break;
      case 3:
        monster = new Monster("Serpent nocturne", 10, 150, 22, 18, 350);
        break;
      case 4:
        monster = new Monster("Dragon de mer", 15, 555, 54, 63, 834);
        break;
      case 5:
        menu();
        break;
    }
    Fight();
  }

  public void startFight() {
    itemList.add(healthPotion);
    itemList.add(forcePotion);
    itemList.add(defPotion);
    itemList.add(superPotion);
    player.setCurrentHP(player.getMaxHP());
    monster.setCurrentHP(monster.getMaxHP());
  }

  public void battlePhase() {
    while (player.getCurrentHP() > 0 || monster.getCurrentHP() > 0) {
      System.out.println("--------------------------Action--------------------------");
      System.out.println("1. Attaquer");
      System.out.println("2. Objets");
      System.out.println("3. Fuir");

      switch (scanner.nextInt()) {
        case 1:
          atk();
          break;
        case 2:
          Items();
          break;
        case 3:
          System.out.println(player.getName() + " à fuit le combat");
          selectMonster();
          break;
      }
    }
  }

  // Appeller la methode Fight
  public void Fight() {
    startFight();
    battlePhase();

  }

  public void atk() {
    int damages;
    System.out.println(player.getName() + " attaque " + monster.getName());
    damages = player.getAtk() - monster.getDef();
    if (damages < 0) {
      damages = 1;
    }
    monster.setCurrentHP(monster.getCurrentHP() - damages);
    System.out.println(player.getName() + " à infligé " + damages + " à " + monster.getName());
    System.out.println(monster.getName() + "--->HP restants--->" + monster.getCurrentHP());
    System.out.println(monster.getName() + " attaque " + player.getName());
    damages = monster.getAtk() - player.getDef();
    if (damages <= 0) {
      damages = 1;
    }
    player.setCurrentHP(player.getCurrentHP() - damages);
    System.out.println(monster.getName() + " à infligé " + damages + " à " + player.getName());
    System.out.println(player.getName() + "--->HP restants--->" + player.getCurrentHP());
    if (player.getCurrentHP() <= 0) {
      System.out.println("Vous êtes morts!!!!");
      menu();

    } else if (monster.getCurrentHP() <= 0) {
      System.out.println(player.getName() + " à battu " + monster.getName() + "!!!!!!");
      checkNextLvl();
      selectMonster();
    }
  }

  public void choiceName() {
    System.out.println("Veuillez entrer un nom: ");
    player.setName(scanner.next());
    System.out.println("Bienvenue " + player.getName() + " !");
  }

  public void choiceClass() {
    System.out.println("Veuillez choisir une classe !");
    System.out.println("1----->Guerrier !");
    System.out.println("2----->Mage !");
    System.out.println("3----->Assassin !");
    System.out.println("4----->Archer !");

    switch (scanner.nextInt()) {
      case 1:
        player = new Player("Guerrier", player.getName(), 123, 8, 9);
        break;
      case 2:
        player = new Player("Mage", player.getName(), 80, 14, 7);
        break;
      case 3:
        player = new Player("Assassin", player.getName(), 90, 10, 8);
        break;
      case 4:
        player = new Player("Archer", player.getName(), 110, 11, 9);
        break;
    }
    System.out.println("Vous avez choisi " + player.getClassName());
    System.out.println("Voici vos stats actuelles");
    System.out.println("Classe : " + player.getClassName() + " Nom : " + player.getName() + " PV : " + player.getMaxHP()
        + " Attaque : " + player.getAtk() + " Défense : " + player.getDef());
  }

  public void checkNextLvl() {
    player.setXp(player.getXp() + monster.getXp());
    System.out.println("Vous avez aquis " + player.getXp() + " points d'experience");
    if (player.getXp() >= player.getXpNextLvl()) {
      player.setLvl(player.getLvl() + 1);
      System.out.println("Bravo!!! " + player.getName() + " est monté au niveau" + player.getLvl());
      player.setXpNextLvl(player.getXpNextLvl() * 1.2);
      stattsNextLvl();
    }
  }

  public void stattsNextLvl() {
    System.out.println("Voici vos nouvelles stats !!!");
    if (player.getClassName() == "Guerrier") {
      player.setMaxHP(player.getMaxHP() + 50);
      player.setAtk(player.getAtk() + 2);
      player.setDef(player.getDef() + 3);
    } else if (player.getClassName() == "Mage") {
      player.setMaxHP(player.getMaxHP() + 20);
      player.setAtk(player.getAtk() + 6);
      player.setDef(player.getDef() + 2);
    } else if (player.getClassName() == "Assassin") {
      player.setMaxHP(player.getMaxHP() + 40);
      player.setAtk(player.getAtk() + 4);
      player.setDef(player.getDef() + 2);
    } else if (player.getClassName() == "Archer") {
      player.setMaxHP(player.getMaxHP() + 45);
      player.setAtk(player.getAtk() + 5);
      player.setDef(player.getDef() + 3);
    }
    System.out
        .println("Classe : " + player.getClassName() + " Niveau : " + player.getLvl() + " PV : " + player.getMaxHP()
            + " Attaque : " + player.getAtk() + " Défense : " + player.getDef());
  }

  public void Items() {

    System.out.println("--------------------------Objets--------------------------");
    if (itemList.isEmpty()) {
      System.out.println("Vous n'avez aucun objet à utiliser");
    } else {
      for (int i = 0; i < itemList.size(); i++) {
        System.out
            .println(
                "Nombre de " + itemList.get(i).getNameItem() + " restants :" + itemList.get(i).getCurrentNumberitem());
      }
      for (int i = 0; i < itemList.size(); i++) {
        System.out
            .println(i + 1 + "--> Utiliser une " + itemList.get(i).getNameItem());
      }
      System.out.println(itemList.size() + 1 + "--> Annuler");
      for (int i = 0; i < itemList.size(); i++) {
        switch (scanner.nextInt()) {
          case 1:
            if (player.getCurrentHP() == player.getMaxHP()) {
              System.out.println("Hp déja au max");
            } else if (player.getCurrentHP() + itemList.get(i).gethealthEffect() > player.getMaxHP()) {
              System.out.println(player.getName() + " à recuperé "
                  + (player.getCurrentHP() + itemList.get(i).gethealthEffect() - player.getMaxHP()) + "HP");
              player.setCurrentHP(player.getCurrentHP() + itemList.get(i).gethealthEffect() - player.getMaxHP());
              itemList.get(i).setCurrentNumberitem(itemList.get(i).getCurrentNumberitem() - 1);
            } else if (player.getCurrentHP() < player.getMaxHP()) {
              player.setCurrentHP(player.getCurrentHP() + itemList.get(i).gethealthEffect());
              System.out.println(player.getName() + " à recuperé "
                  + itemList.get(i).gethealthEffect() + "HPsss");
              itemList.get(i).setCurrentNumberitem(itemList.get(i).getCurrentNumberitem() - 1);
            }
            if (itemList.get(i).getCurrentNumberitem() <= 0)
              itemList.remove(i);
            Items();
          case 2:
            player.setAtk(player.getAtk() + itemList.get(i).getatkEffect());
            if (itemList.get(i).getCurrentNumberitem() <= 0)
              itemList.remove(i);
            Items();
          case 3:
            player.setDef(player.getDef() + itemList.get(i).getdefEffect());
            if (itemList.get(i).getCurrentNumberitem() <= 0)
              itemList.remove(i);
            Items();
          case 4:
            player.setCurrentHP(player.getCurrentHP() + itemList.get(i).gethealthEffect() - player.getMaxHP());
            player.setAtk(player.getAtk() + itemList.get(i).getatkEffect());
            player.setDef(player.getDef() + itemList.get(i).getdefEffect());
            if (itemList.get(i).getCurrentNumberitem() <= 0)
              itemList.remove(i);
            Items();
          case 5:
            battlePhase();
        }
      }
    }
  }
}